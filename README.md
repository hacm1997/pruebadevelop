# BOOKS DEVELOP TEST

<h5>Steps to run apps on the server</h5>

<h3>First Step</h3>

<p>Enter the path where the apps are located:</p>

<code> cd /root/pruebadevelop </code>

<h5>For the Json capture app, enter:</h5>

<code>cd JsonDev</code>

<p>Then, we proceed to activate the environment and run the app on the server with:</p>

<code>source entorno/bin/activate</code>

<code>python3 app.py</code>

<p>The app will be launched on the server with ip http://0.0.0.0:5000/, but since unicorn is being used you will have to enter the following url in your browser:</p>

<code>http://45.79.139.51:5000/</code>

<h5>For the Django app, enter (positioning itself in the folder 'pruebadevelop'):</h5>

<code>cd inventario</code>

<p>Then, we proceed to activate the environment and run the app on the server with:</p>

<code>source entorno/bin/activate</code>

Enter to the file 'Books':

<code>cd Books</code>

<p>Gunicorn is also used to launch the app on the server:</p>

<code>gunicorn --bind 0.0.0.0:8000 cbvlibrary.wsgi:application</code>

<p>Again, you must enter the following url where the app will be launched:</p>

<code>http://45.79.139.51:8000/</code>

<h3>¡End!</h3>