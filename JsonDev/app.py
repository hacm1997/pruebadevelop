from flask import Flask, render_template
from flask import request
import requests
from flask_bootstrap import Bootstrap
from flask_mysqldb import MySQL
import json

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'libros'

Bootstrap(app)
mysql = MySQL(app)

url = 'https://raw.githubusercontent.com/bvaughn/infinite-list-reflow-examples/master/books.json'

response = requests.get(url)

response_json = response.json()

@app.route('/')
def index():
    return render_template('index.html')
@app.route('/hola',methods=['GET', 'POST'])	
def hola():
  
  for i in range(0, 5):
    title = response_json[i]['title']
    isbn = response_json[i]['isbn']

  return render_template('savebd.html', m = request.method, r = response_json)

@app.route('/save',methods=['GET', 'POST'])	
def save():
  for i in range(0, 5):
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO book(name, isbn_number) VALUES (%s, %s)", (response_json[i]['title'], response_json[i]['isbn']))
    mysql.connection.commit()
    cur.close()
  
  msn = "¡Success!"

  return render_template('index.html', m = request.method, mn = msn)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')